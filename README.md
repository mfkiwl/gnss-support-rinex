# Tools for manipulating RINEX files

## Building a Development Environment

This project uses [Nix](https://nixos.org/nix) to manage build environments
for both development workstations and BitBucket Pipelines. See
geoscienceaustralia/project0-nix.

1) Install Nix

```bash
curl https://nixos.org/nix/install | sh
```
2) In your projects root directory, enter nix shell to download and install the
required software as specified in `./shell.nix`

```bash
nix-shell
```

## Managing Nix Dependencies

Edit `shell.nix` to add or remove dependencies. Use
[Niv](https://github.com/nmattia/niv) to manage
[nixpkgs](https://github.com/NixOS/nixpkgs) and any other sources of nix
expressions. Regularly cache any changes to project dependencies by manually
running custom pipeline `build-pipelines-docker-image`.
