package au.gov.ga.gnss.support.rinex;

import java.io.BufferedInputStream;
import java.io.InputStream;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;

/**
 * Decompress RINEX files.
 * <p>
 * Decompress any format, compress using GZIP.
 * </p>
 */
public class RinexCompression {

    private static final CompressorStreamFactory compressorStreams = CompressorStreamFactory.getSingleton();

    /**
     * Decompress anything.
     * <p>
     * In case of unrecognised compression format, return the input &mdash; perhaps
     * it is uncompressed. For list of supported compression formats see
     * {@code org.apache.commons.compress.compressors.CompressorStreamFactory}.
     * </p>
     */
    public static InputStream decompress(InputStream compressedRinexData) {
        try {
            InputStream buffered = compressedRinexData.markSupported()
                ? compressedRinexData
                : new BufferedInputStream(compressedRinexData);

            try {
                CompressorStreamFactory.detect(buffered);
            }
            catch (CompressorException e) {
                // This can happen if stream compression is unrecognised, so let's hope
                // for the best and assume the stream is uncompressed
                return buffered;
            }
            return compressorStreams.createCompressorInputStream(buffered);
        }
        catch (CompressorException e) {
            throw new BadRinexFileHeaderException("Failed to uncompress rinex stream.", e);
        }
    }
}

