package au.gov.ga.gnss.support.rinex;

import static au.gov.ga.gnss.support.rinex.HeaderField.decimal;
import static au.gov.ga.gnss.support.rinex.HeaderField.string;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import com.google.common.base.Joiner;

import lombok.Getter;

/**
 * Description of a single RINEX header line
 */
class HeaderLine {

    public static final HeaderLine rinexVersion = new HeaderLine(
        "^(?<%s>[0-9\\. ]{9})([ ]{11})(?<fileType>.)(.{19})(?<satelliteSystem>.)(.{19}RINEX VERSION / TYPE$)",
        string(1, "rinexVersion", "RINEX VERSION", 9));

    public static final HeaderLine markerName = new HeaderLine(
        "^(?<%s>.{60})(.{0}MARKER NAME$)",
        string(1, "markerName", "MARKER NAME", -60));

    public static final HeaderLine markerNumber = new HeaderLine(
        "^(?<%s>.{20})(.{40}MARKER NUMBER$)",
        string(1, "markerNumber", "MARKER NUMBER", -20));

    public static final HeaderLine observerAndAgency = new HeaderLine(
        "^(?<%s>.{20})(?<%s>.{40})(.{0}OBSERVER / AGENCY$)",
        string(1, "observer", "OBSERVER", -20),
        string(2, "agency", "AGENCY", -40));

    public static final HeaderLine receiverId = new HeaderLine(
        "^(?<%s>.{20})(?<%s>.{20})(?<%s>.{20})(.{0}REC # / TYPE / VERS$)",
        string(1, "receiverSerialNumber", "REC #", -20),
        string(2, "receiverType", "REC TYPE", -20),
        string(3, "receiverFirmwareVersion", "REC VERS", -20));

    public static final HeaderLine antennaId = new HeaderLine(
        "^(?<%s>.{20})(?<%s>.{20})(.{20}ANT # / TYPE$)",
        string(1, "antennaSerialNumber", "ANT #", -20),
        string(2, "antennaType", "ANT TYPE", -20));

    public static final HeaderLine antennaMarkerArp = new HeaderLine(
        "^(?<%s>[\\-0-9\\. ]{14})(?<%s>[\\-0-9\\. ]{14})(?<%s>[\\-0-9\\. ]{14})(.{18}ANTENNA: DELTA H/E/N$)",
        decimal(1, "antennaMarkerArp.left", "ANTENNA DELTA H", 14),
        decimal(2, "antennaMarkerArp.middle", "ANTENNA DELTA E", 14),
        decimal(3, "antennaMarkerArp.right", "ANTENNA DELTA H", 14));

    public static final HeaderLine approxPositionXyz = new HeaderLine(
        "^(?<%s>[\\-0-9\\. ]{14})(?<%s>[\\-0-9\\. ]{14})(?<%s>[\\-0-9\\. ]{14})(.{18}APPROX POSITION XYZ$)",
        decimal(1, "approxPositionXyz.left", "APPROX POSITION X", 14),
        decimal(2, "approxPositionXyz.middle", "APRROX POSITION Y", 14),
        decimal(3, "approxPositionXyz.right", "APRROX POSITION Z", 14));

    public static final HeaderLine comment = new HeaderLine(
        "^(?<%s>.{60})(.{0}COMMENT$)",
        string(1, "comment", "COMMENTS", -60));

    public static final HeaderLine endOfHeader = new HeaderLine(
        "^[ ]{60}END OF HEADER$");

    /**
     * Regex pattern for a complete header line
     */
    private Pattern pattern;

    /**
     * Header line template, 60 spaces followed by a header line identifier
     */
    private String template;

    /**
     * Descriptions of header fields in this line
     */
    @Getter
    private HeaderField<?>[] fields;

    private HeaderLine(String pattern, HeaderField<?>... fields) {
        String[] groups = Stream.of(fields)
            .map(field -> field.getGroupName())
            .toArray(size -> new String[size]);

        this.template = new String(
            new char[60]).replace('\0', ' ') +
            pattern.substring(pattern.lastIndexOf('}') + 1, pattern.lastIndexOf('$'));

        this.pattern = Pattern.compile(String.format(pattern, (Object[]) groups));
        this.fields = fields;
    }

    /**
     * Parse the supplied input line.
     */
    public Matcher parseLine(String line) {
        return this.pattern.matcher(line);
    }

    /**
     * Parse header fields from a line of input into the supplied header object.
     * Leave header object unmodified if the input does not match this header line.
     */
    public boolean parseLine(RinexFileHeader header, String line) {
        Matcher matcher = this.parseLine(line);
        if (matcher.matches()) {
            Stream.of(this.fields).forEach(field -> field.setValue(header, matcher));
        }
        return matcher.matches();
    }

    /**
     * Generate a header line with field values from the supplied header object.
     */
    public String generateLine(RinexFileHeader header) {
        return this.overwriteLine(header, this.template);
    }

    /**
     * Overwrite a header line with field values from the supplied header object.
     * Return the input unmodified if it doesn't match this header line.
     */
    public String overwriteLine(RinexFileHeader header, String line) {
        Matcher matcher = this.pattern.matcher(line);
        if (matcher.matches()) {
            int groupCount = matcher.toMatchResult().groupCount();
            if (groupCount == 0) {
                return line;
            }
            List<String> replacementFieldValues = new ArrayList<>();
            for (int i = 0; i < groupCount; i++) {
                replacementFieldValues.add("$" + (i + 1));
            }
            boolean emptyLine = true;
            for (HeaderField<?> field : this.fields) {
                if (field.getValue(header) != null) {
                    String replacementValue = String.format("%" + field.getPadding() + "s", field.getFormattedValue(header));
                    replacementFieldValues.set(field.getGroupNumber() - 1, replacementValue);
                    emptyLine = false;
                }
            }
            if (emptyLine) {
                return null;
            } else {
                String replacementLine = matcher.replaceAll(Joiner.on("").join(replacementFieldValues));
                if (!this.parseLine(replacementLine).matches()) {
                    throw new BadRinexFileHeaderException(
                            "Overwritting would result in an invalid header line:"
                            + " '" + replacementLine + "'"
                            + " does not match regex " + this.pattern
                            );
                }
                return replacementLine;
            }
        } else {
            return line;
        }
    }
}
